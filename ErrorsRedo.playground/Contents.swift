import Foundation
import XCTest


let ErrorDomainTypeKey: String = "DomainTypeKey"
let ErrorDomainCodeKey: String = "DomainCodeKey"
let ErrorDomainRequestUUIDKey: String = "DomainRequestUUIDKey"

public protocol CustomErrorCodeConvertible {
    var customErrorCode: String { get }
}

extension CustomErrorCodeConvertible {
    public var customErrorCode: String {
        return Mirror(reflecting: self).children.first?.label ?? String(describing: self)
    }
}

extension CustomNSError where Self: CustomErrorCodeConvertible {
    public var defaultErrorUserInfo: [String : Any] {
        [ErrorDomainTypeKey: String(describing:  type(of: self)),
         ErrorDomainCodeKey: customErrorCode]
    }
    public var errorUserInfo: [String : Any] { defaultErrorUserInfo }
}

extension CustomErrorCodeConvertible where Self: CustomStringConvertible {
    public var description: String { return "\(String(describing:  type(of: self))) \(customErrorCode)" }
}

extension LocalizedError {
    // Prefer meaningful, related content in both title and message fields.         [Description, Recovery Suggestion]
    // If there's no recovery info, use a generic title and push description down.  [Generic Title, Description]

    func title(fallbackDescription: String? = nil) -> String {
        let localizedString = "Some Default"

        if recoverySuggestion != nil {
            return errorDescription ?? fallbackDescription ?? localizedString
        } else {
            return localizedString
        }
    }

    func message(fallbackDescription: String? = nil) -> String? {
        if let recoverySuggestion = recoverySuggestion {
            return recoverySuggestion
        } else {
            return errorDescription ?? fallbackDescription
        }
    }
}


public enum GeneralServiceError: CustomErrorCodeConvertible {
    /// An error that was unable to be refined to a more specific domain error
    /// or not recognized by all parties involved.
    case untranslatedError(_ error: Error)
    
    /// The error code is unknown, but we want to pull the title and message provided up to the client.
    case userFacingError(_ title: String?, _ message: String?, _ rawData: Data?)
}

extension GeneralServiceError: CustomNSError {
    public var errorUserInfo: [String : Any] {
        
        let underlyingUserInfo: [String: Any] = {
            switch self {
            case .untranslatedError(let error):
                let nsError = error as NSError
                return nsError.userInfo
            case .userFacingError(let title, let message, let rawData):
                let rawAsString = rawData.map { String(data: $0, encoding: .utf8) } ?? String(describing: self)
                return ["GeneralServiceError.userFacingError.title": title ?? "",
                        "GeneralServiceError.userFacingError.message": message ?? "",
                        "GeneralServiceError.userFacingError.rawData": rawAsString ?? ""]
            
            }
        }()
        
        return defaultErrorUserInfo.merging(underlyingUserInfo) { (current, _) in current }
    }
}

extension GeneralServiceError: LocalizedError {
    /// A localized message describing what error occurred.
    public var recoverySuggestion: String? {
        "Oops, something went wrong"
    }
    
    public var errorDescription: String?{
        "Oops Title"
    }
}

extension GeneralServiceError: CustomStringConvertible {
    public var description: String {
        switch self {
        case .untranslatedError(let error):
            return "Untranslated Error: \(String(describing: error))"
        default:
            return String(describing: self)
        }
    }
}

extension GeneralServiceError {
    var isTranslatable: Bool {
        // If this error is of raw type then return true
        return false
    }
}

public enum CartServiceError: CustomErrorCodeConvertible, CustomNSError, CustomStringConvertible {
    case creditCardExpired
    case alcoholUnavailableForDelivery(String?)
    case itemUnavailableDueToLimit(_ title: String, _ message: String?)
}


extension CartServiceError: LocalizedError {
    /// A localized message describing what error occurred.
    public var recoverySuggestion: String? {
        switch self {
        case .itemUnavailableDueToLimit(_, let message):
            return message
        default:
            return "Cart Message"
        }
    }

    /// A localized title describing what error occurred.
    public var errorDescription: String? {
        switch self {
        case .itemUnavailableDueToLimit(let title, _):
            return title
        default:
            return "Cart Title"
        }
    }
}

public struct HEBServiceError  {
    public let underlyingError: Error
    
    private let userInfo: [String: Any]
    
    public init(underlyingError: Error, additionalUserInfo: [String: Any] = [:]) {
        self.underlyingError = underlyingError
        
        let nsError = underlyingError as NSError
        var mergedUserInfo = nsError.userInfo
        mergedUserInfo = mergedUserInfo.merging(
            additionalUserInfo) { current, _ in current }
        
        self.userInfo = mergedUserInfo
    }
}

extension HEBServiceError: CustomNSError {
    public var errorUserInfo: [String : Any] { userInfo }
}

extension HEBServiceError: CustomStringConvertible {
    public var description: String { String(describing: underlyingError) }
}

extension HEBServiceError: LocalizedError {
    /// A localized message describing what error occurred.
    public var recoverySuggestion: String? {
        guard let localizedError = underlyingError as? LocalizedError else { return nil }
        return localizedError.recoverySuggestion
    }
    
    public var errorDescription: String? {
        guard let localizedTitleError = underlyingError as? LocalizedError else { return nil }
        return localizedTitleError.errorDescription
    }
}

class ErrorTests: XCTestCase {
    func testGeneralServiceError() throws {
        let randomError = NSError(domain: "Domain", code: 4, userInfo: ["Key1": "Value1"])
        
        let generalServiceError = GeneralServiceError.untranslatedError(randomError)
        let hebServiceError = HEBServiceError(underlyingError: generalServiceError)

        let nsError = hebServiceError as NSError
        
        // want the internal untranslated code as the description
        XCTAssertEqual(nsError.description, "Untranslated Error: Error Domain=Domain Code=4 \"(null)\" UserInfo={Key1=Value1}")
        
        // want the under
        let sortedUserInfo = nsError.userInfo.sorted(by: { $0.0 < $1.0 })
        XCTAssertEqual(sortedUserInfo.description, "[(key: \"DomainCodeKey\", value: \"untranslatedError\"), (key: \"DomainTypeKey\", value: \"GeneralServiceError\"), (key: \"Key1\", value: \"Value1\")]")
        
        let localizedError = try XCTUnwrap(nsError as? LocalizedError)
        XCTAssertEqual(localizedError.title(), "Oops Title")
        XCTAssertEqual(localizedError.message(), "Oops, something went wrong")
        
        if case GeneralServiceError.untranslatedError = hebServiceError.underlyingError  {
        } else {
            XCTFail()
        }
    }
    
    func testCartServiceError() throws {
        let cartError = CartServiceError.itemUnavailableDueToLimit("Too Much", "Items")
        
        let hebServiceError = HEBServiceError(underlyingError: cartError, additionalUserInfo: [ErrorDomainRequestUUIDKey: "A123"])

        let nsError = hebServiceError as NSError
        
        // want the internal untranslated code as the description
        XCTAssertEqual(nsError.description, "CartServiceError itemUnavailableDueToLimit")
        
        let sortedUserInfo = nsError.userInfo.sorted(by: { $0.0 < $1.0 })
        XCTAssertEqual(sortedUserInfo.description, "[(key: \"DomainCodeKey\", value: \"itemUnavailableDueToLimit\"), (key: \"DomainRequestUUIDKey\", value: \"A123\"), (key: \"DomainTypeKey\", value: \"CartServiceError\")]")
        
        let localizedError = try XCTUnwrap(nsError as? LocalizedError)
        XCTAssertEqual(localizedError.title(), "Too Much")
        XCTAssertEqual(localizedError.message(), "Items")
        
        if case CartServiceError.itemUnavailableDueToLimit = hebServiceError.underlyingError  {
        } else {
            XCTFail()
        }
    }
}

ErrorTests.defaultTestSuite.run()
